FROM openjdk:8-jre-alpine

RUN mkdir -p /opt/app

WORKDIR /opt/app

COPY . .


CMD java -jar target/scala-2.12/simple-api-scala-assembly-1.0.0.jar